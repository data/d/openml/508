# OpenML dataset: nflpass

https://www.openml.org/d/508

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Roger W. Johnson      
**Source**: [StatLib](http://lib.stat.cmu.edu/datasets/)  
**Please cite**:  

**National Football League Passes**  
Dataset listing all-time NFL passers through 1994 by the NFL passing efficiency rating.
Associated passing statistics from which this rating is computed are included.

The dataset lists statistics for 26 players. The first 25 are the top 25 all-time career best rating leaders recognized by the NFL. The 26th player, Otto Graham, has statistics which include his performance in the All-America Football Conference (1946-1949) which is not recognized by the NFL. The statistics given are current through the 1994 regular season. Only passers with a minimum of 1,500 career passing attempts are included.

The NFL describes how to compute its rating in its 1977 document "National Football League Passer Rating System" (410 Park Avenue, New York, NY 10022-4444, (212) 758-1500) through the use of tables. No formula is explicitly stated for rating. But, examining the tables in the "National
Football League Passer Rating System" one can infer that NFL passer rating is

[5(Completion Percentage-30)/6] + [10(Touchdown Percentage)/3] +
[25(19-2(Interception Percentage))/12] + [25(Yards/Attempts-3)/6]

where it is understood that the values within each set of square brackets are
truncated to be no smaller than zero and no larger than 475/12. This implies a
minimal rating of 0 and a maximal rating of 475/3 or about 158.3. If

30%  &lt;     Completion Percentage   &lt; 77.5%
0%   &lt;     Touchdown Percentage    &lt; 11.875%
0%   &lt;     Interception Percentage &lt;  9.5%
3   &lt;         Yards per Attempt   &lt; 12.5,

which is true of most passers having a reasonable number of passing attempts,
then the rating formula simplifies to

[25 + 10(Completion Percentage) + 40(Touchdown Percentage)
- 50(Interception Percentage) + 50(Yards/Attempt)]/12

(see Johnson (1993, 1994). Note that the weights on interception percentage and
yards per attempt are greatest in magnitude, closely followed by touchdown
percentage. The weight on completion percentage is a distant fourth in
magnitude.


### Classroom Use of this Data   
Using the NFL data from Meserole (1995), for which the above inequalities
hold, one can uncover (at least approximately) the simplified rating formula
using multiple regression. Students can be told that NFL rating is "based on
performance standards established for completion percentage, average gain,
touchdown percentage and interception percentage" (Meserole (1995)), but the
actual formula for rating is not widely publicized. Once the rating formula is
uncovered, one can see the relative weights that the NFL assigns to these four
performance standards (see Barra and Neyer (1995) for an alternative). Also, by
citing unusual passers who don't satisfy the above inequalities an instructor
can remind students of the dangers of extrapolation when building regression
models. Here are a few such unusual passers:

Name   Attempts Completions Yards Touchdowns Interceptions Rating

Rypien    3          3       15       0            0        87.5
Marshall  1          1       81       1            0       158.3
Muster    1          0        0       0            1         0.0

The data for Arthur Marshall, a wide-receiver for Denver, and for Brad Muster,
a full-back for Chicago are from the 1992 season. The data for quarterback Mark
Rypien is for his performance at one point during the 1995 season (see _USA
Today_, Thursday September 28, 1995, 9C).

One might also try tracking down the passing (not receiving!) records of Jerry
Rice for the 1995 season as he apparently threw for a touchdown in the regular
season finale.

### Variables (from left to right)  
Passing Attempts  
Passing Completions  
Passing Yards  
Touchdowns by Passing  
Interceptions  
NFL Rating (usually to the nearest tenth, sometimes to the nearest hundredth to eliminate ties that would result when only given to the nearest tenth)  
Name of NFL Player  


### References
Barra, A. and Neyer, R. (1995), "When rating quarterbacks, yards per throw
matters", _The Wall Street Journal_, Friday, November 24, B5.

Johnson, R. (1994), "Rating quarterbacks: An amplification", _The College
Mathematics Journal_, vol. 25, no. 4, p. 340.

Johnson, R. (1993), "How does the NFL rate the passing ability of
quarterbacks?", _The College Mathematics Journal_, vol. 24, no. 5, pp. 451-453.

Meserole, M., editor, (1995), "The 1996 Information Please Sports Almanac",
p. 265.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/508) of an [OpenML dataset](https://www.openml.org/d/508). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/508/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/508/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/508/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

